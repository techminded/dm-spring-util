package net.dm.util.service;

import net.hiwidget.service.MessageByLocaleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;



@Service
public class MailService {

    private static final Logger logger = LoggerFactory.getLogger(MailService.class);

    @Value("${hiwidget.mail.from}")
    private String mailFrom;

    @Value("${hiwidget.ui.notification.email}")
    private String siteNotificationEmail;

    @Autowired
    TemplateRenderService templateRenderService;

    @Autowired
    private MessageByLocaleService messageByLocaleService;

    @Value("${hiwidget.ui.title}")
    private String siteTitle;


    public void sendSiteNotification(String subjectId, String templateId, Map<String, String> data) {

        sendEmail(siteNotificationEmail, subjectId, templateId, data);
    }

    public void sendEmail(String to, String subjectId, String templateId, Map<String, String> data) {

        String subject = messageByLocaleService.getMessage(subjectId);
        String htmlContent = templateRenderService.renderTemplate(templateId, data);

        JavaMailSenderImpl sender = new JavaMailSenderImpl();
        sender.setDefaultEncoding("UTF-8");
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        try {
            helper.setFrom(mailFrom);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(htmlContent, true);

            sender.send(message);
        } catch (Exception e) {
            logger.error("Error sending notification email", e.getMessage());
        }
    }

    public void sendOrderAdminNotification(Map<String, String> data) throws Exception {

        this.sendSiteNotification("order.mail.title", "classpath:/templates/email/banner_order_submitted", data);
    }

    public void sendTicketAdminNotification(String client, String banner, String name, String phone, String email, String page) throws Exception {

        Map<String, String> data = new HashMap<String, String>() {{
            put("client", client);
            put("banner", banner);
            put("name", name);
            put("phone", phone);
            put("email", email);
            put("page", page);
        }};

        this.sendSiteNotification("order.mail.title", "classpath:/templates/email/ticket_submitted", data);
    }

    public void sendHelpRequestNotification(String phone) {
        Map<String, String> data = new HashMap<String, String>() {{
            put("site", siteTitle);
            put("phone", phone);
        }};

        this.sendSiteNotification("panel.mail.helprequest.subject", "classpath:/templates/email/helprequest_submitted", data);
    }

    public void sendCodeRequest(String email) {
        Map<String, String> data = new HashMap<String, String>() {{
            put("site", siteTitle);
            put("email", email);
        }};

        this.sendEmail(email, "panel.mail.sendcode.subject", "classpath:/templates/email/send_code", data);
    }

    public void sendPaymentAdminNotification(Map<String, String> data) {

        this.sendSiteNotification("payment.mail.subject", "classpath:/templates/email/payment_submitted", data);
    }
}

