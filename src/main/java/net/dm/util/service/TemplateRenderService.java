package net.dm.util.service;

import net.hiwidget.service.MessageByLocaleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Map;


@Service
public class TemplateRenderService {

    private static final Logger logger = LoggerFactory.getLogger(TemplateRenderService.class);

    @Autowired
    TemplateEngine templateEngine;

    @Autowired
    private MessageByLocaleService messageByLocaleService;

    public String renderTemplate(String templateId, Map<String, String> parameters) {

        Locale locale = LocaleContextHolder.getLocale();
        ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = sra.getRequest();

        final WebContext ctx = new WebContext(request, sra.getResponse(), request.getServletContext(), locale);

        for (String parameterKey : parameters.keySet()) {
            ctx.setVariable(parameterKey, parameters.get(parameterKey));
        }

        String htmlContent = templateEngine.process(templateId, ctx);
        return htmlContent;
    }

}

